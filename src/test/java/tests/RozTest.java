package tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.Page;


import java.util.concurrent.TimeUnit;

public class RozTest {

    private static WebDriver driver;


    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "src\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/apple_iphone_x_64gb_silver/p22726294/");
    }
    @Test
    public void duckTest() throws Exception{
        Page page = new Page(driver);

        page.checkName();

        page.addPhone();
        page.checkCart();

        TimeUnit.SECONDS.sleep(1);

        page.contPurch();
        page.checkName();

        page.openCart();
        page.checkCart();

        TimeUnit.SECONDS.sleep(1);

        page.delPhone();
        page.checkEmpty();

        page.closeCart();
        page.checkName();

        page.openCart();
        page.checkEmpty();

        page.closeCart();
        page.returnToPhone();
        driver.navigate().refresh();

        page.openCart();
        page.checkEmpty();


    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
