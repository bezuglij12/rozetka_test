package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Page {
    private static WebDriver driver;

    @FindBy(xpath = "//h1[contains(text(),'Apple iPhone X 64GB Silver')]")
    private WebElement checkName1;

    @FindBy(name="topurchases")
    private WebElement addButton;

    @FindBy(css="span.btn-link.btn-link-gray > a[name=\"close\"]")
    private WebElement contPurch;

    @FindBy(xpath = "//a[@onclick=\"document.fireEvent('openCart', {extend_event: [{name: 'eventLocation', value: 'Head'}]});\"]")
    private WebElement openCart;

    @FindBy(css = "img.cart-check-icon.sprite")
    private WebElement cartCheck;

    @FindBy(name = "delete")
    private WebElement deleteButton;

    @FindBy(css = "img.popup-close-icon.sprite")
    private WebElement popupClose;

    @FindBy(xpath = "//a[contains(text(),'Apple iPhone X 64GB Silver')]")
    private WebElement checkName2;

    @FindBy(css = "h2.empty-cart-title")
    private WebElement checkEmpty;

    @FindBy(css = "a.novisited.nav-tabs-link")
    private WebElement returnToPhone;



    public Page(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public boolean checkName(){
        return checkName1.isDisplayed();
    }
    public boolean checkCart(){
        return checkName2.isDisplayed();
    }
    public boolean checkEmpty(){
        return checkEmpty.isDisplayed();
    }

    public Page addPhone(){
        addButton.click();
        return new Page(driver);
    }

    public Page contPurch(){
        contPurch.click();
        return new Page(driver);
    }

    public Page openCart(){
        openCart.click();
        return new Page(driver);
    }

    public Page delPhone(){
        cartCheck.click();
        deleteButton.click();
        return new Page(driver);
    }

    public Page closeCart(){
        popupClose.click();
        return new Page(driver);
    }

    public Page returnToPhone(){
        returnToPhone.click();
        return new Page(driver);
    }



}

